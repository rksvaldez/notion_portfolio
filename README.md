# Ryan Khalil Valdez

Hi, I'm Ryan Khalil Valdez, and I am currently a Flutter developer trainee at FFUF Manila for almost a month. 

We have been using Javascript and Dart programming language. I've been interested to learn Flutter to create mobile applications someday.

# Contact Information

Linkedin: [linkedin.com/in/ryankhalilvaldez](https://www.linkedin.com/in/ryan-khalil-valdez-47138a1b3/)

Website Portfolio: [behance.net/ryankhalilvaldez](https://www.behance.net/ryankhalilvaldez)

# Work Experience

Flutter Developer Trainee

FFUF Manila, From July 2021 to August 2021

- I was trained to know how Flutter works
- I have worked with a pet project for Task List app for FFUF Manila

# Skills

Technical skills

- Javascript
- Dart
- Flutter
- C++
- VB.Net
- HTML
- CSS

Soft skills

- Team Player
- Creative

# Education

University of Perpetual Help System - Biñan, 2017-2020